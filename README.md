Right now, this may need a couple of things to work.

First off I think it needs copies of the three files {ca.crt, namespace, token}
from the `/var/run/secrets/kubernetes.io/serviceaccount/` directory from within
the inventory service pod.  I did this by doing:

```
mkdir -p /var/run/secrets/kubernetes.io/serviceaccount 
# Adjust permissions to this directory as necessary so that your kubectl user can write

kubectl -n osaconfig exec -it <inventory-service-pod> cat /var/run/secrets/kubernetes.io/serviceaccount/ca.crt > /var/run/secrets/kubernetes.io/serviceaccount/ca.crt
kubectl -n osaconfig exec -it <inventory-service-pod> cat /var/run/secrets/kubernetes.io/serviceaccount/namespace > /var/run/secrets/kubernetes.io/serviceaccount/namespace
kubectl -n osaconfig exec -it <inventory-service-pod> cat /var/run/secrets/kubernetes.io/serviceaccount/token > /var/run/secrets/kubernetes.io/serviceaccount/token

The inventory service pod name can be aquired by running kubectl -n osaconfig get pods
```

The IP of the inventory-service service is also needed for the cli to connect to `kubectl -n osaconfig get services`