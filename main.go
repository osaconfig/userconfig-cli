package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"

	micro "github.com/micro/go-micro"
	k8s "github.com/micro/kubernetes/go/micro"
	invPb "gitlab.com/osaconfig/protobufs/userconfig"
)

const (
	defaultAddress = "localhost"
	microSelector  = "static"
	microRegistry  = "kubernetes"
)

var containerBridgeDict = map[string]string{
	"BRDUMMY":    "br-dummy",
	"BRMGMT":     "br-mgmt",
	"BRVXLAN":    "br-vxlan",
	"BRPROVIDER": "br-provider",
	"BRVLAN":     "br-vlan",
	"BRFLAT":     "br-flat",
	"BRSTORAGE":  "br-storage",
	"BRLBAAS":    "br-lbaas",
}

var groupBindDict = map[string]string{
	"ALLCONTAINERS":           "all_containers",
	"HOSTS":                   "hosts",
	"NEUTRONOVNCONTROLLER":    "neutron_ovn_controller",
	"NEUTRONLINUXBRIDGEAGENT": "neutron_linuxbridge_agent",
	"UTILITYALL":              "utility_all",
	"GLANCEAPI":               "glance_api",
	"CINDERAPI":               "cinder_api",
	"CINDERVOLUME":            "cinder_volume",
	"NOVACOMPUTE":             "nova_compute",
	"SWIFTPROXY":              "swift_proxy",
	"CEPHOSD":                 "ceph-osd",
	"OCTAVIAWORKER":           "octavia_worker",
	"OCTAVIAHOUSEKEEPING":     "octavia_housekeeping",
	"OCTAVIAHEALTHMONITOR":    "octavia_health_monitor",
}

var anchorBlockDict = map[string]string{
	"CINDER":       "cinder_block",
	"COMPUTE":      "compute_block",
	"INFRA":        "infra_block",
	"LOADBALANCER": "loadbalancer_block",
	"LOG":          "log_block",
	"SWIFT":        "swift_block",
	"CEPHOSDS":     "ceph_osds_block",
}

func envSet(key, fallback string) error {
	_, lu := os.LookupEnv(key)
	if !lu {
		os.Setenv(key, fallback)
	}
	return nil
}

func main() {
	// Setup Environment Variables
	envSet("MICRO_SELECTOR", microSelector)
	envSet("MICRO_REGISTRY", microRegistry)
	envSet("INVENTORY_SERVICE_IP", defaultAddress)

	service := k8s.NewService(micro.Name("osaconfig.cli.inventoryservice"))
	service.Init()

	client := invPb.NewUserConfigServiceClient(os.Getenv("INVENTORY_SERVICE_IP"), service.Client())

	res, err := client.GetUserConfig(context.Background(), &invPb.GetRequest{Environment: "testing-1"})
	if err != nil {
		log.Fatalf("Could not get inventory: %v", err)
	}

	// This needs a function
	var cidrYaml strings.Builder
	cidrYaml.WriteString("cidr_networks:\n")
	cidrs := res.UserConfig.GetCidrNetworks()
	for _, n := range cidrs {
		fmt.Fprintf(&cidrYaml, "  %s: %s/%s\n", n.Id, n.CidrNetwork, n.CidrNetmask)
	}

	// This needs a function
	var usedIPyaml strings.Builder
	usedIPyaml.WriteString("used_ips:\n")
	usedips := res.UserConfig.GetUsedIps()
	for _, i := range usedips {
		fmt.Fprintf(&usedIPyaml, "  - \"%s,%s\"\n", i.UsedipsStart, i.UsedipsEnd)
	}

	var globalOverrideYaml strings.Builder
	globalOverrideYaml.WriteString("global_overrides:\n")
	globalOverrides := res.UserConfig.GetGlobalOverrides()
	fmt.Fprintf(&globalOverrideYaml, "  internal_lb_vip_address: \"%v\"\n", globalOverrides.InternalLbVipAddress)
	fmt.Fprintf(&globalOverrideYaml, "  external_lb_vip_address: \"%v\"\n", globalOverrides.ExternalLbVipAddress)
	fmt.Fprintf(&globalOverrideYaml, "  tunnel_bridge: \"%v\"\n", containerBridgeDict[globalOverrides.TunnelBridge.String()])
	fmt.Fprintf(&globalOverrideYaml, "  management_bridge: \"%v\"\n", containerBridgeDict[globalOverrides.ManagementBridge.String()])
	fmt.Fprintf(&globalOverrideYaml, "  provider_networks:\n")
	providerNets := globalOverrides.GetProviderNetworks()
	for _, n := range providerNets {
		fmt.Fprintf(&globalOverrideYaml, "    - network:\n")
		fmt.Fprintf(&globalOverrideYaml, "      container_bridge: \"%v\"\n", containerBridgeDict[n.ContainerBridge.String()])
		fmt.Fprintf(&globalOverrideYaml, "      container_type: \"%v\"\n", n.ContainerType)
		fmt.Fprintf(&globalOverrideYaml, "      container_interface: \"%v\"\n", n.ContainerInterface)
		fmt.Fprintf(&globalOverrideYaml, "      type: \"%v\"\n", n.Type)
		if n.GetIpFromQ() != "" {
			fmt.Fprintf(&globalOverrideYaml, "      ip_from_q: \"%v\"\n", n.IpFromQ)
		}
		if n.GetRange() != "" {
			fmt.Fprintf(&globalOverrideYaml, "      range: \"%v\"\n", n.Range)
		}
		if n.GetNetName() != "" {
			fmt.Fprintf(&globalOverrideYaml, "      net_name: \"%v\"\n", n.NetName)
		}
		fmt.Fprintf(&globalOverrideYaml, "      is_container_address: %v\n", n.IsContainerAddress)
		fmt.Fprintf(&globalOverrideYaml, "      is_container_address: %v\n", n.IsSshAddress)
		fmt.Fprintf(&globalOverrideYaml, "      group_binds:\n")
		groupBinds := n.GetGroupBinds()
		for _, g := range groupBinds {
			fmt.Fprintf(&globalOverrideYaml, "        - %v\n", groupBindDict[g.String()])
		}
	}
	fmt.Fprint(&globalOverrideYaml, "  swift:\n")
	swiftOverrides := globalOverrides.GetSwiftGlobalOverrides()
	for _, s := range swiftOverrides {
		fmt.Fprintf(&globalOverrideYaml, "    part_power: %v\n", s.PartPower)
		fmt.Fprintf(&globalOverrideYaml, "    storage_network: '%v'\n", containerBridgeDict[s.StorageNetwork.String()])
		fmt.Fprintf(&globalOverrideYaml, "    replication_network: '%v'\n", containerBridgeDict[s.ReplicationNetwork.String()])
		fmt.Fprintf(&globalOverrideYaml, "    drives: \n")
		swiftDrives := s.GetDrives()
		for _, sd := range swiftDrives {
			fmt.Fprintf(&globalOverrideYaml, "      - name: %v\n", sd.Name)
		}
		fmt.Fprintf(&globalOverrideYaml, "    mount_point: %v\n", s.MountPoint)
		storagePolicies := s.GetStoragePolicies()
		fmt.Fprintf(&globalOverrideYaml, "    storage_policies:\n")
		for _, sp := range storagePolicies {
			fmt.Fprintf(&globalOverrideYaml, "      - policy: \n")
			fmt.Fprintf(&globalOverrideYaml, "        name: %v\n", sp.Name)
			fmt.Fprintf(&globalOverrideYaml, "        index: %v\n", sp.Index)
			fmt.Fprintf(&globalOverrideYaml, "        default: %v\n", sp.Default)
		}
	}

	anchorBlocks := res.UserConfig.GetAnchorBlocks()
	for _, ab := range anchorBlocks {
		fmt.Fprintf(&globalOverrideYaml, "%v: &%v\n", anchorBlockDict[ab.AnchorName.String()], anchorBlockDict[ab.AnchorName.String()])
		hosts := ab.GetHost()
		for _, host := range hosts {
			fmt.Fprintf(&globalOverrideYaml, "  %v:\n", host.HostName)
			fmt.Fprintf(&globalOverrideYaml, "    ip: %v\n", host.Ip)
			fmt.Fprintf(&globalOverrideYaml, "    container_vars:\n")
			containerVars := host.GetContainerVars()
			for _, cv := range containerVars {
				fmt.Fprintf(&globalOverrideYaml, "      container_tech: %v\n", cv.ContainerTech)
				if cv.GetStorageBackend() != nil {
					storageBackend := cv.GetStorageBackend()
					if storageBackend.GetCinderBackend() != nil {
						cinderBackend := storageBackend.GetCinderBackend()
						fmt.Fprintf(&globalOverrideYaml, "      cinder_backends:\n")
						fmt.Fprintf(&globalOverrideYaml, "        limit_container_types: %v\n", cinderBackend.LimitContainerTypes)
						fmt.Fprintf(&globalOverrideYaml, "        lvm: \n")
						//TODO(d34dh0r53): Figure out a way to recurse through these structures
						lvm := cinderBackend.GetLvm()
						fmt.Fprintf(&globalOverrideYaml, "          volume_group: %v\n", lvm.VolumeGroup)
						fmt.Fprintf(&globalOverrideYaml, "          volume_driver: %v\n", lvm.VolumeDriver)
						fmt.Fprintf(&globalOverrideYaml, "          volume_backend_name: %v\n", lvm.VolumeBackendName)
						fmt.Fprintf(&globalOverrideYaml, "          volume_group: %v\n", lvm.IscsiIpAddress)
					}
				}
			}
		}
	}

	fmt.Fprintf(&globalOverrideYaml, "shared-infra_hosts: *infra_block\n")
	fmt.Fprintf(&globalOverrideYaml, "repo-infra_hosts: *infra_block\n")
	fmt.Fprintf(&globalOverrideYaml, "log_hosts: *log_block\n")
	fmt.Fprintf(&globalOverrideYaml, "haproxy_hosts: *loadbalancer_block\n")
	fmt.Fprintf(&globalOverrideYaml, "os-infra_hosts: *infra_block\n")
	fmt.Fprintf(&globalOverrideYaml, "identity_hosts: *infra_block\n")
	fmt.Fprintf(&globalOverrideYaml, "storage-infra_hosts: *infra_block\n")
	fmt.Fprintf(&globalOverrideYaml, "storage_hosts: *cinder_block\n")
	fmt.Fprintf(&globalOverrideYaml, "image_hosts: *infra_block\n")
	fmt.Fprintf(&globalOverrideYaml, "compute-infra_hosts: *infra_block\n")
	fmt.Fprintf(&globalOverrideYaml, "compute_hosts: *compute_block\n")
	fmt.Fprintf(&globalOverrideYaml, "orchestration_hosts: *infra_block\n")
	fmt.Fprintf(&globalOverrideYaml, "dashboard_hosts: *infra_block\n")
	fmt.Fprintf(&globalOverrideYaml, "network_hosts: *infra_block\n")
	fmt.Fprintf(&globalOverrideYaml, "swift_hosts: *swift_block\n")
	fmt.Fprintf(&globalOverrideYaml, "swift-proxy_hosts: *infra_block\n")

	fmt.Println("---")
	fmt.Println(cidrYaml.String())
	fmt.Println(usedIPyaml.String())
	fmt.Println(globalOverrideYaml.String())
}
